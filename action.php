<?php
/**
 * DokuWiki Plugin structgeohash (Action Component)
 *
 * @author: Andrés Seré <andres@sere.uy>
 */

# I don't known whats this:
#use dokuwiki\plugin\structgeohash\meta\ApplicationAspectKernel;

// must be run within Dokuwiki
if (!defined('DOKU_INC')) die();

class action_plugin_structgeohash extends DokuWiki_Action_Plugin {
    /**
     * Registers a callback function for a given event
     *
     * @param Doku_Event_Handler $controller DokuWiki's event controller object
     * @return void
     */
  public function register(Doku_Event_Handler $controller) {
    $controller->register_hook('PLUGIN_STRUCT_TYPECLASS_INIT', 'BEFORE', $this, 'handle_init');
		//$controller->register_hook('TPL_METAHEADER_OUTPUT', 'BEFORE', $this, '_hookjs');
  }


	/**
	* Hook geohash js library into page headers.
	*
	*/
	public function _hookjs(Doku_Event $event, $param) {
		$event->data['script'][] = array(
			'type'    => 'text/javascript',
			'charset' => 'utf-8',
			'_data'   => '',
			'src'     => DOKU_PLUGIN.'structgeohash/geohash.js');
  }


  public function handle_init(Doku_Event &$event, $param) {
    $event->data['GeoHash'] = 'dokuwiki\\plugin\\structgeohash\\types\\GeoHash';
  }
}
// vim:ts=4:sw=4:et:
