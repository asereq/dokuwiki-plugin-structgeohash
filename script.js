
/* DOKUWIKI:include geohash.js */


struct_gh_maps = new Array();
struct_gh_markers = new Array();

jQuery( document ).ready(function() {

/* 
 * For each element of class 'struct_geohash_map'
 * initializes de leaflet map and so on:
 * */
jQuery(".struct_geohash_map").each(function() {
	// id is something like 'xxxx_map'
	var id = jQuery(this).attr('id');
	// the data for the leaflet library was writen to the data- tags:
	var urlTMP = jQuery(this).attr('data-urlTMP');
	var attribution = jQuery(this).attr('data-attribution');
	var maxZoom = jQuery(this).attr('data-maxZoom');
	var mapId = jQuery(this).attr('data-mapId');

	// erase last 4 characters ('_map'):
	var inputId = id.slice(0,-4);

	// get the value of the input and decode de geohash:
	var gh_default = jQuery("#"+inputId).attr('value');
	var decoded = decodeGeoHash(gh_default);

	// Initalizes the map:
	var map = L.map(id).setView([decoded.latitude[2], decoded.longitude[2]], 13);

	L.tileLayer(urlTMP, {
		attribution: attribution,
		maxZoom: maxZoom,
		id: mapId,
		tileSize: 512,
		zoomOffset: -1,
	}).addTo(map);

	// Put the marker:
	var marker = L.marker([decoded.latitude[2], decoded.longitude[2]]).addTo(map);

	// If the edition is enabled, register a function to change the marker
	if ( ! jQuery('#'+inputId).prop('disabled') ) {
		map.on('click', function (e) { onMapClick(e, inputId); });
	}
	
	// Save the maps on a struct:
	struct_gh_maps[inputId] = map;
	struct_gh_markers[inputId] = marker;
});

});

// On click, updates the input field and the marker position
function onMapClick(e, id) {
	struct_gh_markers[id].setLatLng(e.latlng);
	mygh = encodeGeoHash(e.latlng.lat, e.latlng.lng);
	jQuery("#"+id).attr('value',mygh);
}
